package scripts.fc.missions.fcdoricsquest.tasks;

import org.tribot.api.General;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.interaction.impl.npcs.NpcDialogue;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fcdoricsquest.data.DoricSettings;

public class DoricDialogue extends Task
{
	private static final long serialVersionUID = -2799862046107080194L;
	
	private final Positionable DORIC_TILE = new RSTile(2951, 3451, 0);
	private final int DISTANCE_THRESHOLD = 2;

	@Override
	public void execute()
	{
		if(Player.getPosition().distanceTo(DORIC_TILE) > DISTANCE_THRESHOLD)
			WebWalking.walkTo(DORIC_TILE);
		else
			if(new NpcDialogue("Talk-to", "Doric", 10, 0, 0, 1).execute())
				General.sleep(600, 1800);

	}

	@Override
	public boolean shouldExecute()
	{
		return DoricSettings.FINISH_QUEST.isValid() || DoricSettings.START_QUEST.isValid();
	}

	@Override
	public String getStatus()
	{
		return "Doric dialogue";
	}

}
